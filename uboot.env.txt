arch=arm
baudrate=115200
board_name=3 Model B
board_rev=0x8
board_rev_scheme=1
board_revision=0xA22082
baseargs=root=/dev/ram rootfstype=ramfs init=/init
bootdelay=3
bootenv=uEnv.txt
cpu=armv7
fdt_addr_r=0x00000100
fdtaddr=0x02000000
fdtfile=bcm2710-rpi-3-b.dtb
filesize=1c5
envloadaddr=0x00200000
loadfdt=fdt addr 0x02000000; fdt get value args /chosen bootargs
loadaddr=0x01000000
rd_addr=0x02100000
findmmc=part size mmc 0 2 config_size; if test "${config_size}" = "800"; then part start mmc 0 2 config_start; setenv mmcdata 3; mmc read $envloadaddr ${config_start} ${config_size}; env import $envloadaddr 0x1000; else setenv mmcdata 2; fi
loadargs=setenv bootargs "${baseargs} pv_try=${pv_try} pv_rev=${boot_rev} panic=2 ${args} ${configargs} ${localargs}"
loadenv_mmc=load mmc ${mmcdev}:${mmcdata} ${envloadaddr} /boot/uboot.txt;
loadenv_ubi=ubifsload ${envloadaddr} /boot/uboot.txt;
loadenv=if test "${pv_fstype}" = "ubi"; then loadenv_ubi; else run loadenv_mmc; fi; setenv pv_try; env import ${envloadaddr} 0x400; if env exists pv_try; then if env exists pv_trying && test ${pv_trying} = ${pv_try}; then setenv pv_trying; saveenv; setenv boot_rev ${pv_rev}; else setenv pv_trying ${pv_try}; saveenv; setenv boot_rev ${pv_trying}; fi; else setenv boot_rev ${pv_rev}; fi;
loadstep_mmc=run loadenv; load mmc ${mmcdev}:${mmcdata} ${loadaddr} /trails/${boot_rev}/.pv/pv-kernel.img; load mmc ${mmcdev}:${mmcdata} ${rd_addr} /trails/${boot_rev}/.pv/pv-initrd.img; setenv rd_size ${filesize}; setexpr rd_offset ${rd_addr} + ${rd_size}; setenv i 0; while load mmc ${mmcdev}:${mmcdata} ${rd_offset} /trails/${boot_rev}/.pv/pv-initrd.img.${i}; do setexpr i ${i} + 1; setexpr rd_size ${rd_size} + ${filesize}; setexpr rd_offset ${rd_offset} + ${filesize}; done
loadstep_ubi=ubi part ${pv_ubipart}; ubifsmount ubi0:${pv_ubipart}; run loadenv; ubifsload ${loadaddr} /trails/${boot_rev}/.pv/pv-kernel.img; ubifsload ${rd_addr} /trails/${boot_rev}/.pv/pv-initrd.img;
loadstep=if test "${pv_fstype}" = "ubi"; then loadstep_ubi; else run loadstep_mmc; fi; setenv rd_size ${filesize}
bootcmd=run findmmc; run loadstep; run loadfdt; run loadargs; bootm ${loadaddr} ${rd_addr}:${rd_size} ${fdtaddr}; echo "Failed to boot step, rebooting"; sleep 1; reset
pv_fstype=mmc
mmcdev=0
mmcboot=1
mmcdata=2
serial#=000000006522d565
soc=bcm2835
stderr=serial,lcd
stdin=serial,usbkbd
stdout=serial,lcd
vendor=raspberrypi

